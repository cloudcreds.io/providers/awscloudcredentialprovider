module gitlab.com/cloudcreds.io/awscloudcredentialprovider

go 1.14

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/hashicorp/go-hclog v0.13.0 // indirect
	github.com/hashicorp/go-plugin v1.3.0
	github.com/hashicorp/yamux v0.0.0-20190923154419-df201c70410d // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mitchellh/go-testing-interface v1.14.1 // indirect
	github.com/oklog/run v1.1.0 // indirect
	github.com/rs/zerolog v1.18.0
	gitlab.com/cloudcreds.io/cloudcredentials v0.0.0-20200525010010-339ed0ba8691
	gitlab.com/trukoda.com/enumerations v0.0.0-20200524072000-0f2a51630435
	golang.org/x/net v0.0.0-20200520182314-0ba52f642ac2 // indirect
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20200521103424-e9a78aa275b7 // indirect
	google.golang.org/grpc v1.29.1 // indirect
)
