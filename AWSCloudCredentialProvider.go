package main

import (
	"encoding/json"
	"fmt"
	"github.com/hashicorp/go-plugin"
	"github.com/rs/zerolog"
	plugs "gitlab.com/cloudcreds.io/cloudcredentials/pkg/provider"
	. "gitlab.com/trukoda.com/enumerations"
	"os"
)

// CredentialPersistenceProvider FILL ME OUT
type CredentialPersistenceProvider struct {
	log              *zerolog.Logger
	supportedFormats Enumerator
	defaultFormat    string
}

// NewCloudCredentialPersistenceProvider constructor to create the AWS CredentialPersistenceProvider
func NewCloudCredentialPersistenceProvider(logger *zerolog.Logger) CredentialPersistenceProvider {
	_formats := NewEnumerator(Element{ID: 0, Value: "FILE"})
	_ = _formats.AddElement(Element{ID: 1, Value: "ENVIRONMENT"})

	// Return sane defaults
	return CredentialPersistenceProvider{
		log:              logger,
		supportedFormats: *_formats,
		defaultFormat:    _formats.Value(0),
	}
}

type Credential struct {
	Data struct {
		AccessKey     string `json:"access_key"`
		SecretKey     string `json:"secret_key"`
		SecurityToken string `json:"security_token"`
	}
}

func (aws CredentialPersistenceProvider) Persist(input plugs.PersistInput) string {
	if aws.supportedFormats.Value(input.Format) == "FILE" {
		var c Credential
		aws.log.Debug().Msgf("Data coming in was %s", input.Data)
		_ = json.Unmarshal(input.Data, &c)
		aws.log.Debug().Msg("Unmarshalled the Data Structure")
		aws.log.Debug().Msgf("Data is %s", c)
		aws.log.Debug().Msgf("AccessKey is %s", c.Data.AccessKey)
		aws.log.Debug().Msgf("SecretKey is %s", c.Data.SecretKey)
	} else if aws.supportedFormats.Value(input.Format) == "ENVIRONMENT" {
		aws.log.Debug().Msg("Formatting for environment Variables")
	} else {
		panic("unsupported format requested")
	}
	fmt.Println(input.Data)
	return ""
}

// GetSupportedFormats FILL ME OUT
func (aws CredentialPersistenceProvider) GetSupportedFormats(input string) Enumerator {
	return aws.supportedFormats
}

var handshakeConfig = plugin.HandshakeConfig{
	ProtocolVersion:  1,
	MagicCookieKey:   "BASIC_PLUGIN",
	MagicCookieValue: "hello",
}

func main() {
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()
	aws := NewCloudCredentialPersistenceProvider(&logger)

	// pluginMap is the map of plugins we can dispense.
	var pluginMap = map[string]plugin.Plugin{
		"cred": &plugs.CredentialPersistencePlugin{Impl: aws},
	}

	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: handshakeConfig,
		Plugins:         pluginMap,
	})

}
